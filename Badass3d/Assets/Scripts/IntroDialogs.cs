﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IntroDialogs : MonoBehaviour {
	public string[] dialogs;
	public int dialogIndex = 0;
	// Use this for initialization

	void Start(){
		GetComponent<Text> ().text = dialogs [dialogIndex];
		}

	void Update(){
		if(Input.GetButtonDown("Submit"))
		   NextDialog();
	}

	public void NextDialog(){
		dialogIndex++;
		indexValidation ();
		GetComponent<Text> ().text = dialogs [dialogIndex];
	}

	void indexValidation(){
		if (dialogIndex > dialogs.Length - 1) {
						dialogIndex = dialogs.Length - 1;
						GetComponent<LoadLevel> ().loadlvl ("Chap1");
				}
	}
}
