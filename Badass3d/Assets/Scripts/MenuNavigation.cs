﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuNavigation : MonoBehaviour {
	public EventSystem evSystem;
	public Button[] menuButtons;
	public int index;
	// Use this for initialization
	void Start () {
		//evSystem = EventSystem.FindObjectOfType (typeof(EventSystem)) as EventSystem;
		//evSystem = EventSystemManager.currentSystem;
		evSystem = EventSystem.current;
	}
	
	// Update is called once per frame
	void Update () {
		if (GetComponent<PauseMenu> ().Paused) {
						if (Input.GetAxisRaw ("Vertical") == -1) {
								indexadd ();
								evSystem.SetSelectedGameObject (menuButtons [index].gameObject, null);
						} else if (Input.GetAxisRaw ("Vertical") == 1) {
								indextake ();
								evSystem.SetSelectedGameObject (menuButtons [index].gameObject, null);
						}
						if(Input.GetButton("Accept")){
				menuButtons[index].gameObject.GetComponent<Button>().onClick.Invoke();
						}
				}
	}

	void indexadd(){
		if (index >= menuButtons.Length -1)
			index = menuButtons.Length - 1;
				else
						index++;
	}

	void indextake(){
		if (index <= 0)
			index = 0;
		else
			index--;
	}
}
