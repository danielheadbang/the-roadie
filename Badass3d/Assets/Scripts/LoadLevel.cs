﻿using UnityEngine;
using System.Collections;

public class LoadLevel : MonoBehaviour {

	public void loadlvl(string nam)
	{
		Application.LoadLevel (nam);
	}

	public void exit(){
		Application.Quit ();
	}

	public void restartlvl(){
		Application.LoadLevel (Application.loadedLevel);
	}

	public void restartlastlvl(){
		Application.LoadLevel(PlayerPrefs.GetString("last lvl"));
	}

	public void loadnextlvl(){
		Application.LoadLevel ((Application.loadedLevel + 1));
	}

	public void loadpreviouslvl(){
		Application.LoadLevel ((Application.loadedLevel - 1));
	}

	public void restartvalues(){
		PlayerPrefs.DeleteAll ();
	}
}
