﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {
	public Transform Player;
	public float MoveSpeed;
	public float distance;
	public bool meleeRange;
	public float scaleXReset = 8.472277f;
	public int damageDone;
	public int DistanceMultiplier;

	// Use this for initialization
	void Start () {
		meleeRange = false;
		Player = GameObject.Find("Killmaster").transform;
	}
	
	// Update is called once per frame
	void Update () {
			if ( Vector3.Distance( Player.position, transform.position ) < distance )
			{
				if(meleeRange){
					transform.position = transform.position;
				}else{
					transform.position += ( transform.position - Player.position ).normalized * -MoveSpeed * Time.deltaTime;
					GetComponentInChildren<Animator>().SetBool("Walking",true);
				}
			}
			else
			{
			transform.position = transform.position;
			GetComponentInChildren<Animator>().SetBool("Walking",false);
			}

		if (Player.position.x > transform.position.x) {
			transform.localScale = new Vector3 (scaleXReset, transform.localScale.y, transform.localScale.z);
		} 
		else {
			transform.localScale = new Vector3 (-scaleXReset, transform.localScale.y, transform.localScale.z);
		}
		//WHEN ROCK POWER IS ACTIVATED
		if ((Vector3.Distance (Player.position, transform.position) < distance) && Player.GetComponent<Attacks> ().powerActivated
		    && this.name != "Sven")
						GetComponent<EnemyHealth> ().curHealth = 0;
	}

	void OnCollisionEnter(Collision col){
		if (col.transform.tag == "Player") {
			transform.position = transform.position;
			meleeRange = true;
			GetComponentInChildren<Animator>().SetBool("Walking",false);
			GetComponent<Timer> ().timesUp = true;
		}
	}
	void OnCollisionStay(Collision colls){
		if (colls.transform.tag == "Player") {
			if(!colls.transform.GetComponent<Attacks>().isAttacking)
				GetComponent<Timer>().started = true;
			else
				GetComponent<Timer>().started = false;

			if(GetComponent<Timer>().timesUp){
				if(this.name == "Sven" && GetComponent<EnemyHealth>().curHealth <= GetComponent<EnemyHealth>().maxHealth/2)
					GetComponentInChildren<Animator> ().SetTrigger ("BellySlam");
				else
					GetComponentInChildren<Animator> ().SetTrigger ("Attacking");
				colls.transform.GetComponent<Health>().ChangeHealth(-damageDone);
				colls.transform.GetComponent<Collisions>().KnockBack(this.transform);
			}
		}
	}

	void OnTriggerStay(Collider target){
		if (target.tag == "Player" && target.GetComponent<Attacks>().isAttacking) {
			KnockBack(Player);
		}
	}

	void OnCollisionExit(Collision coll){
		if (coll.transform.tag == "Player") {
			meleeRange = false;
				}
	}

	void KnockBack(Transform enemy){
		if (this.transform.position.x < enemy.position.x)
			GetComponent<Rigidbody>().AddForce (Vector3.left * DistanceMultiplier);
		else
			GetComponent<Rigidbody>().AddForce (Vector3.right * DistanceMultiplier);
	}
}