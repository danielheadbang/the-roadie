﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {

	public int maxHealth;
	public int curHealth;
	public Vector3 DeadCollider;
	public float timeToDie = 1f;
	//private float healthBarLength;
	

	void Start () {
		//healthBarLength = Screen.width / 10;
		curHealth = maxHealth;
	}
	
	
	void Update () {
		AddjustCurrentHealth(0);
		if (curHealth <= 0) {
			GetComponent<BoxCollider>().size = DeadCollider;
			GetComponentInChildren<Animator>().SetTrigger("Dead");
			Destroy(gameObject,timeToDie);
		}

	}
	
	//curHealth + "/" + maxHealth
	/*void OnGUI(){
		//this.gameObject.transform.position.y
		Vector2 targetPos;
		targetPos = Camera.main.WorldToScreenPoint (transform.position);
		GUI.backgroundColor = Color.red;
		if(curHealth>0)
			GUI.Button(new Rect(targetPos.x+gameObject.transform.position.x, Screen.height/this.gameObject.transform.position.y, healthBarLength, 20), "");
	}*/
	
	
	public void AddjustCurrentHealth(int adj) {
		curHealth += adj;	
		
		if(curHealth < 0)
			curHealth = 0;
		
		if(curHealth > maxHealth)
			curHealth = maxHealth;
		
		if(maxHealth < 1)
			maxHealth = 1;
		
		//healthBarLength = (Screen.width / 10) * (curHealth / (float)maxHealth);
	}

}
