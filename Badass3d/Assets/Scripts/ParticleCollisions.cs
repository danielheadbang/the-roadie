﻿using UnityEngine;
using System.Collections;

public class ParticleCollisions : MonoBehaviour {
	void OnParticleCollision(GameObject obj){
		obj.GetComponent<EnemyHealth> ().AddjustCurrentHealth (-1);
		obj.GetComponentInChildren<Animator> ().SetTrigger ("Hurt");
	}
}
