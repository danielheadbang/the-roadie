﻿using UnityEngine;
using System.Collections;

public class Collisions : MonoBehaviour {
	public bool inRange;
	public Color Inmune, normality;
	public float knockbackDistance;
	public float destroyTime = 0.5f;
	public int forceMultiplier;

	Rigidbody body;

	private void Awake()
	{
		body = GetComponent<Rigidbody>();
	}

	void Start(){
		inRange = false;
	}
	void Update(){
		if(body.IsSleeping())
			body.WakeUp ();
		if(GetComponent<Timer>().timesUp)
			GetComponentInChildren<SpriteRenderer> ().color = normality;
	}
	void OnTriggerEnter(Collider col){
		if (col.tag == "floor") {
			GetComponent<CharController>().canJump = true;
			GetComponentInChildren<Animator>().SetBool("InAir",false);
		}
		if (col.tag == "enemy") {
			inRange = true;
			GetComponent<Attacks>().enemyInRange = col.transform;
		}
	}
	void OnTriggerExit(Collider obj){
		if (obj.tag == "enemy") {
			inRange = false;
			GetComponent<Attacks>().enemyInRange = null;
		}
	}
	void OnCollisionEnter(Collision coll){
		if (coll.transform.tag == "enemy") {
			GetComponent<Timer>().started = true;
			GetComponent<Timer>().timesUp = false;

			if(!GetComponentInChildren<Animator> ().GetCurrentAnimatorStateInfo(0).IsName("Playing") 
			   && !GetComponentInChildren<Animator> ().GetCurrentAnimatorStateInfo(0).IsName("Punching")
			   && !GetComponentInChildren<Animator> ().GetCurrentAnimatorStateInfo(0).IsName("Hardpunch"))
			{
				if(GetComponentInChildren<SpriteRenderer> ().color == normality && coll.transform.GetComponent<EnemyHealth>().curHealth>0)
				{
				GetComponentInChildren<Animator>().SetTrigger("Hurt");
				GetComponentInChildren<SpriteRenderer> ().color = Inmune;
				}
			}//Fight validation
		}

		if (coll.transform.tag == "Extra") {
			coll.transform.GetComponent<ParticleSystem>().Play();
			Destroy(coll.gameObject, destroyTime);
		}

		if (coll.transform.name == "End") {
			GetComponentInChildren<LoadLevel>().loadnextlvl();
			PlayerPrefs.SetFloat("Rock",GetComponent<Attacks>().rockSlider.value);
			PlayerPrefs.SetFloat("Health",GetComponent<Health>().currentHealth);

				}
		if (coll.transform.name == "Return") {
			GetComponentInChildren<LoadLevel>().loadpreviouslvl();
		}
		
	}

	public void KnockBack(Transform enemy){
		if (this.transform.position.x < enemy.position.x)
			GetComponent<Rigidbody>().AddForce (Vector3.left * forceMultiplier);
		else
			GetComponent<Rigidbody>().AddForce (Vector3.right * forceMultiplier);
	}
}