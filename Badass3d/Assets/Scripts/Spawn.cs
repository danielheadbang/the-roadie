﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {
	public GameObject obj1;
	public Transform spwnpt;
	public float StartTime;
	public float RepeatEvery;
	public int MaxSpawns;
	public GameObject[] Spawned;

	void Awake(){
		InvokeRepeating ("up", StartTime, RepeatEvery);
	}

	void Update(){
		Spawned = GameObject.FindGameObjectsWithTag (obj1.tag);
		if (Spawned.Length ==  MaxSpawns)
			CancelInvoke ("up");
	}


	void up(){
		GameObject p = Instantiate (obj1, spwnpt.position, Quaternion.identity) as GameObject;
	}
	
}