﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Attacks : MonoBehaviour {
	public AudioClip Bass,Solo,punch;
	public bool canPlay;
	public bool canAttack;
	public bool isAttacking;
	//public int playTimes = 0, maxPlayTimes;
	public float curTime = 0, triggerTime;
	public int punchCount = 0;
	public Transform enemyInRange;
	private Camera maincam;
	public Slider rockSlider;
	public bool RockPowerUp;
	public bool powerActivated;
	public int heal = 10;
	public int pointsPerKill = 2;
	
	// Use this for initialization
	void Start () {
		maincam = GameObject.Find ("Main Camera").GetComponent<Camera>();
		GetComponentInChildren<ParticleSystem>().Stop();
		canPlay = true;
		canAttack = true;
		isAttacking = false;
		RockPowerUp = false;
		powerActivated = false;
		rockSlider.value = PlayerPrefs.GetFloat ("Rock");
	}
	
	// Update is called once per frame
	void Update () {
		CooldownTimer ();
		if (canAttack) {
						if (Input.GetButtonDown ("Attack")) {
								isAttacking = true;
								if (GetComponent<Collisions> ().inRange && enemyInRange != null) {
										enemyInRange.GetComponent<AudioSource>().Play();
										enemyInRange.GetComponent<EnemyHealth> ().AddjustCurrentHealth (-5);
										enemyInRange.GetComponentInChildren<Animator>().SetTrigger("Hurt");
										if (enemyInRange.GetComponent<EnemyHealth> ().curHealth == 0) {
												rockSlider.value += pointsPerKill;
												enemyInRange = null;
												GetComponent<Collisions> ().inRange = false;
										}
								}
								if (GetComponentInChildren<Animator> ().GetBool ("InAir") == false)
										Punching ();
								GetComponent<AudioSource>().PlayOneShot (punch);
						}
						if (Input.GetButtonUp ("Attack")){
							isAttacking = false;
						}
				}
		if (canPlay) {
						if (Input.GetButtonDown ("Bass")) {
								GetComponent<CharController>().enabled = false;
								canAttack = false;
								canPlay = false;
								curTime = 0;
								GetComponentInChildren<ParticleSystem>().Play();
								BassSound ();
								if(RockPowerUp){
									GetComponentInChildren<Animator> ().SetBool("RockPower", true);
									powerActivated = true;
									rockSlider.value = 0;
								} else 
									GetComponentInChildren<Animator> ().SetBool ("Playing", true);
						}
				}
		if (rockSlider.value == rockSlider.maxValue) {
						RockPowerUp = true;
						rockSlider.GetComponent<Animator> ().SetBool ("Ready", true);
				} else {
						RockPowerUp = false;
						rockSlider.GetComponent<Animator>().SetBool("Ready",false);
				}
	}

	public void BassSound(){
		maincam.GetComponent<AudioSource>().volume = 0.4f;
		GetComponent<Health> ().ChangeHealth (heal);
		if (RockPowerUp)
			GetComponent<AudioSource>().PlayOneShot (Solo);
		else
			GetComponent<AudioSource>().PlayOneShot(Bass);
	}

	public void CooldownTimer(){
		if (curTime < triggerTime) {
						curTime += Time.deltaTime;
				} else {
			GetComponentInChildren<Animator>().SetBool("Playing",false);
			GetComponentInChildren<Animator> ().SetBool("RockPower", false);
			canPlay = true;
			canAttack = true;
			maincam.GetComponent<AudioSource>().volume = 1f;
			GetComponent<CharController>().enabled = true;
			GetComponentInChildren<ParticleSystem>().Stop();
			powerActivated = false;
				}
	}

	public void Punching(){
		if (punchCount == 0) {
			GetComponentInChildren<Animator> ().SetTrigger ("Punching");
			punchCount++;
		} else if(punchCount == 1) {
			GetComponentInChildren<Animator> ().SetTrigger ("Hardpunch");
			punchCount = 0;
				}
	return;
	}
}
