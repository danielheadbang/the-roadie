﻿using UnityEngine;
using System.Collections;

public class LoadingScreen : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		GetComponent<Timer> ().started = true;
		if (GetComponent<Timer> ().timesUp)
			GetComponent<LoadLevel> ().loadlvl ("IntroCutscene");
	}
}
