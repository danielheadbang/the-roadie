﻿using UnityEngine;
using System.Collections;

public class CharController : MonoBehaviour {
	public float xvel, yvel,zvel;
	public bool canJump;
	// Use this for initialization
	void Start () {
		canJump = false;
	
	}
	
	// Update is called once per frame
	void Update () {
	//Movimiento Horizontal
	if (Input.GetAxis ("Horizontal") > 0) {
						GetComponent<Rigidbody>().velocity = new Vector3 (xvel, GetComponent<Rigidbody>().velocity.y, GetComponent<Rigidbody>().velocity.z);
						transform.localScale = new Vector3 (1, 1, 1);
				} else if (Input.GetAxis ("Horizontal") < 0) {
						GetComponent<Rigidbody>().velocity = new Vector3 (-xvel, GetComponent<Rigidbody>().velocity.y, GetComponent<Rigidbody>().velocity.z);
						transform.localScale = new Vector3 (-1, 1, 1);
				}else{	GetComponent<Rigidbody>().velocity = new Vector3 (0, GetComponent<Rigidbody>().velocity.y, GetComponent<Rigidbody>().velocity.z);
				}
		//Movimiento en Z
		if (Input.GetAxis ("Vertical") > 0) {
			GetComponent<Rigidbody>().velocity = new Vector3 (GetComponent<Rigidbody>().velocity.x, GetComponent<Rigidbody>().velocity.y , zvel);
		} else if (Input.GetAxis ("Vertical") < 0) {
			GetComponent<Rigidbody>().velocity = new Vector3 (GetComponent<Rigidbody>().velocity.x, GetComponent<Rigidbody>().velocity.y , -zvel);
		}else{	GetComponent<Rigidbody>().velocity = new Vector3 (GetComponent<Rigidbody>().velocity.x, GetComponent<Rigidbody>().velocity.y, 0);
		}
		//Salto
		if (canJump) {
			if (Input.GetButtonDown ("Jump")) {
								GetComponent<Rigidbody>().velocity = new Vector3 (GetComponent<Rigidbody>().velocity.x, yvel, GetComponent<Rigidbody>().velocity.z);
								canJump = false;
								GetComponentInChildren<Animator>().SetBool("InAir",true);
						}
				}
		if (GetComponentInChildren<Animator> ().GetBool ("InAir")) {
			if (Input.GetButtonDown ("Attack"))
				GetComponentInChildren<Animator>().SetTrigger("JumpPunch");
				}
		//Validaciones de caminar
		if (Input.GetAxis ("Horizontal") > 0 || Input.GetAxis ("Vertical") > 0) {
						GetComponentInChildren<Animator> ().SetBool ("Walking", true);
				} else if (Input.GetAxis ("Horizontal") < 0 || Input.GetAxis ("Vertical") < 0) {
						GetComponentInChildren<Animator> ().SetBool ("Walking", true);
				}else {
			GetComponentInChildren<Animator> ().SetBool ("Walking", false);
				}

	}
}
