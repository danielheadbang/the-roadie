﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {
	public float currentTime = 0;
	public float maxTime;
	public bool timesUp, started;
	// Use this for initialization
	void Start () {
		timesUp = false;
		started = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (started) {
			timesUp = false;
						if (currentTime <= maxTime) {
								currentTime += Time.deltaTime;
						} else {
								started = false;
								timesUp = true;
								currentTime = 0;
						}
				}
	}
}
