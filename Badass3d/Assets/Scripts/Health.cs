﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Health : MonoBehaviour {
	public Slider HealthBar;
	public int maxHealth;
	public float currentHealth;
	// Use this for initialization
	void Start () {
		HealthBar.maxValue = maxHealth;
		HealthBar.value = PlayerPrefs.GetFloat ("Health",100);
	}
	
	// Update is called once per frame
	void Update () {
		currentHealth = HealthBar.value;
		if (HealthBar.value == 0) {
			GetComponent<BoxCollider>().size = new Vector3(1.9810125f,3.1f,1f);
			GetComponentInChildren<Animator>().SetTrigger("Dead");
			GetComponent<CharController>().enabled = false;
			//send last loaded level
			PlayerPrefs.SetString ("last lvl", Application.loadedLevelName);

			PlayerPrefs.DeleteKey("Rock");
			PlayerPrefs.DeleteKey("Health");
				}
	}

	public void ChangeHealth(int healths){
		HealthBar.value += healths;
	}
}
