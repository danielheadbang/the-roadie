﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour {
	public GameObject pauseMenu;
	public bool Paused;
	public float regularTimeScale = 1f, pausedTimeScale = 0f;

	void Start(){
		Paused = false;
		Time.timeScale = regularTimeScale;
	}
	
	void Update () {
		if (GameObject.Find ("Killmaster")) {
			if (!Paused) {
				if (Input.GetButtonDown ("Cancel")) {
					showmenu ();
					Paused = true;
					}
			} else {
				if (Input.GetButtonDown ("Cancel")) {
					hidemenu ();
					Paused = false;
					}
			}
		}
	}

	public void showmenu(){
				Time.timeScale = pausedTimeScale;
				pauseMenu.SetActive (true);
	}

	public void hidemenu(){
		Paused = false;
		Time.timeScale = regularTimeScale;
		pauseMenu.SetActive (false);
	}	
}
